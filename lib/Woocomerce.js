"use strict";

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Woocomerce = void 0;
//@ts-ignore
const woocommerce_rest_api_1 = __importDefault(require("@woocommerce/woocommerce-rest-api"));
const WooCommerceAPI = woocommerce_rest_api_1.default.default;
require('dotenv').config();
class Woocomerce extends WooCommerceAPI {
  constructor(wooUrl, ck_API_woo, cs_API_woo) {
    super();
    this.wooUrl = wooUrl;
    this.ck_API_woo = ck_API_woo;
    this.cs_API_woo = cs_API_woo;
  }
}
exports.Woocomerce = Woocomerce;
console.log(process.env.WOO_URL);