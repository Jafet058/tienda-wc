"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Woocomerce_1 = require("./classes/Woocomerce");
const producto = {
    create: [
        {
            name: "Prueba1",
            regular_price: 100,
            sku: "prueba-123",
            description: "Descripcion prueba",
            manage_stock: true,
            stock_quantity: 50,
            dimensions: {
                length: "100",
                width: "100",
                height: "100"
            },
            weight: "100",
            categories: [
                {
                    id: 758
                }
            ]
        },
        {
            name: "Prueba2",
            regular_price: 100,
            sku: "prueba-1234",
            description: "Descripcion prueba",
            manage_stock: true,
            stock_quantity: 50,
            dimensions: {
                length: "100",
                width: "100",
                height: "100"
            },
            weight: "100",
            categories: [
                {
                    id: 758
                }
            ]
        },
        {
            name: "Prueba3",
            regular_price: 100,
            sku: "prueba-12345",
            description: "Descripcion prueba",
            manage_stock: true,
            stock_quantity: 50,
            dimensions: {
                length: "100",
                width: "100",
                height: "100"
            },
            weight: "100",
            categories: [
                {
                    id: 758
                }
            ]
        }
    ]
};
const wooObj = new Woocomerce_1.Woocomerce();
wooObj.postProductsBatch(producto)
    .then(res => console.log(res.create[0].id))
    .catch(err => console.log(err));
