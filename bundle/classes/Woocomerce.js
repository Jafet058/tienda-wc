"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Woocomerce = void 0;
//@ts-ignore
const woocommerce_rest_api_1 = __importDefault(require("@woocommerce/woocommerce-rest-api"));
require('dotenv').config();
class Woocomerce extends woocommerce_rest_api_1.default {
    constructor(url = `${process.env.WOO_URL}`, consumerKey = `${process.env.CK_API_WOO}`, consumerSecret = `${process.env.CS_API_WOO}`) {
        super({
            url: url,
            consumerKey: consumerKey,
            consumerSecret: consumerSecret,
        });
        this.url = url;
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
    }
    postProductsBatch(productsArray) {
        const _super = Object.create(null, {
            post: { get: () => super.post }
        });
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const productResponse = yield _super.post.call(this, 'products/batch', productsArray);
                return productResponse;
            }
            catch (error) {
                console.log(error);
                throw new Error(`Error al subir productos:\n`);
            }
        });
    }
}
exports.Woocomerce = Woocomerce;
