import { TecnosinergiaProductI } from "../interfaces/TecnosinergiaProduct.interface";
import axios from 'axios';
import { ApiModel } from "./ApiModel";
require('dotenv').config();

export class Tecnosinergia extends ApiModel<TecnosinergiaProductI>{

    headers = {
        'api-token': process.env.TECNOSINERGIA_TOKEN, 
        'Content-Type': 'application/json',
    }

    constructor(
        public productsApiUrl: string = 'https://api.tecnosinergia.info/v3/item/list',

    ){super()}

    async getAllProducts(): Promise<TecnosinergiaProductI[]>{
        console.log('====Trayendo datos de la API Tecnosinergia====');
        try {
            const res = await axios.get<TecnosinergiaProductI[]>(this.productsApiUrl, {
                headers: this.headers,
            })

            return res.data;
    
        } catch (error) {
            throw error;
        }

    }

    filterProducts(data: TecnosinergiaProductI[]): TecnosinergiaProductI[]{
        return data.filter((product) => product.line === 'Videovigilancia' || product.line === 'CCTV HD' ||
            product.line === 'Discos Duros' || product.line === 'Cableado' || product.line === 'Smart Home' ||
            product.line === 'Energ&iacute;a y Monitores' || product.line === 'Alarmas/Intrusi&oacute;n' || product.category === 'Smart Home' || product.category === 'Discos Duros'
            || product.category === 'Videovigilancia M&oacute;vil' || product.category === 'Cableado Estructurado'
        );
    
    }

}