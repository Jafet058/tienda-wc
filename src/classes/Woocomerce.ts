//@ts-ignore
import WooCommerceAPI from "@woocommerce/woocommerce-rest-api";
import { ProductI } from "../interfaces/Product.interface";
import { WoocomerceI } from "../interfaces/Woocomerce.interface";
import { WooProductI } from "../interfaces/WooProduct.interface";
import { calculatePrice, cortarTexto, quitarAcentosYSimbolos, selectCategory, selectImage } from "../helpers/helpers";
import { TecnosinergiaProductI } from "../interfaces/TecnosinergiaProduct.interface";
require('dotenv').config();

export class Woocomerce extends WooCommerceAPI implements WoocomerceI{

    constructor(
        public url: string = `${process.env.WOO_URL}`,
        public consumerKey: string = `${process.env.CK_API_WOO}`,
        public consumerSecret: string = `${process.env.CS_API_WOO}`,
    ){
        super({
            url: url,
            consumerKey: consumerKey,
            consumerSecret: consumerSecret,
        })
    }

    async postProductsBatch(filteredData: TecnosinergiaProductI[], quantity: number = 50): Promise<WooProductI>{
        try {
            let cont = 0;
            let items = [];
            let productResponse;
            
        for(let product of filteredData){
            if(cont === quantity){
                let productsArray = {
                    create: items
                }
                productResponse = await super.post<WooProductI>('products/batch',productsArray);
                console.log(productResponse);
                
            }
        const cleanDescription = quitarAcentosYSimbolos(product.description)
            items[cont] =
                {
                    name: cortarTexto(product.name),
                    regular_price: calculatePrice(product.sale_price),
                    sku: product.sku,
                    description: cleanDescription,
                    manage_stock: true,
                    stock_quantity: product.stock_total,
                    weight: product.weight,
                    dimensions: {
                        length: product.length, 
                        width: product.width, 
                        height: product.height,
                    },
                    categories: [
                        {
                            id: selectCategory(product) || 16,
                        }
                    ],
                    images : [
                        {
                            src: `${process.env.IMAGES_URL}${selectImage(product)}${product.item_id}.jpg`,

                        }
                    ]
                }
        cont++;
        }
        return productResponse; //Ultima respuesta

        } catch (error) {
            console.log(error);
            throw new Error(`Error al subir productos:\n`)
        }

    }

}
