import { TecnosinergiaProductI } from "../interfaces/TecnosinergiaProduct.interface";

export abstract class ApiModel<T>{
    abstract getAllProducts(): Promise<T[]>;
    abstract filterProducts(data: T[]): T[];
}