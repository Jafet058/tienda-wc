import { TecnosinergiaProductI } from "../interfaces/TecnosinergiaProduct.interface";
import { Create } from "../interfaces/WooProduct.interface"
export const categories = [
    {
        id: 755,
        line: 'Videovigilancia',
    },
    {
        id: 754,
        line: 'CCTV HD',
    },
    {
        id: 756,
        line: 'Discos Duros',
    },
    {
        id: 757,
        line: 'Cableado',
    },
    {
        id: 753,
        line: 'Smart Home',
    },
    {
        id: 758,
        line: 'Infraestructura',
        category: 'Memorias',

    },
    {
        id: 759,
        line: 'Energ&iacute;a y Monitores',
    }

]

export const calculatePrice = (regularPrice: number): string => (regularPrice + (regularPrice * 0.25)).toString();

export const selectCategory = (product: TecnosinergiaProductI) => {
    let cont = 0;
    for (let e of categories) {
        if (e.line == product.line) {
            return e.id;
        }
        cont++;
    }
}

export const cortarTexto = (texto: string, limite: number = 30): string => {
    // Divide el texto en palabras
    let palabras = texto.split(' ');

    if (palabras.length > limite) {
        let palabrasCortadas = palabras.slice(0, limite);
        let textoCortado = palabrasCortadas.join(' ');

        return textoCortado;
    }
    return texto;
}

export const quitarAcentosYSimbolos = (texto: string): string => {
    // Utiliza normalize() para eliminar acentos
    const textoSinAcentos = texto.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    
    // Utiliza replace() para eliminar otros símbolos no deseados
    const textoLimpio = textoSinAcentos.replace(/[^\w\s]/gi, '');
    
    return textoLimpio;
}

export const selectImage = (product: TecnosinergiaProductI) => {
    return (product.line === 'Alarmas/Intrusi&oacute;n') ? 'Smart Home' : product.line;
}