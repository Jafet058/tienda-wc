import { create } from "domain";
import { Tecnosinergia } from "./classes/Tecnosinergia";
import { Woocomerce } from "./classes/Woocomerce";
import { TecnosinergiaProductI } from "./interfaces/TecnosinergiaProduct.interface";

const productsTecnosinergia = [
  {
    item_id:            12,
    sku:                 '123',
    name:                'Prueba',
    description:         'Prueba',
    sale_price:          100,
    sale_price_USD:     5,
    weight:              200,
    stock_total:         50,
    line:                'Prueba',
    width:               100,
    length:              100,
    height:              50,
  },
  {
    item_id:             13,
    sku:                 '1234',
    name:                'Prueba2',
    description:         'Prueba2',
    sale_price:          100,
    sale_price_USD:     5,
    weight:              200,
    stock_total:         50,
    line:                'Prueba2',
    width:               100,
    length:              100,
    height:              50,
  },
]

const tecno = new Tecnosinergia();
tecno.getAllProducts()
.then(tecnoSinergiaProducts => {
  
  const filteredProducts = tecno.filterProducts(tecnoSinergiaProducts)

  const wooObj = new Woocomerce();
  wooObj.postProductsBatch(filteredProducts)

  .then(res => console.log(res.create[0]))
  .catch(err => console.log(err))

})
