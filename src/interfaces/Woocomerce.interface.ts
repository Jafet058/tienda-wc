//Interfaz para la api de Woocommerce
export interface WoocomerceI {
    url: string,
    consumerKey: string,
    consumerSecret: string,
}