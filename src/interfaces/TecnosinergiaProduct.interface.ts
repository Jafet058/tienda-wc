export interface TecnosinergiaProductI {
    item_id:            number;
    sku:                 string;
    name:                string;
    description:         string;
    sale_price:          number;
    sale_price_USD?:     number;
    weight?:              number;
    stock_total:         number;
    line:                string;
    width:               number;
    length:              number;
    height:              number;
    category?:           string;
}
