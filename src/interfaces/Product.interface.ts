//Tipo producto que se sube a wordpress
export interface ProductI {
    create: Create[];
}

export interface Create {
    name:           string;
    regular_price:  number;
    sku:            string;
    description:    string;
    manage_stock:   boolean;
    stock_quantity: number;
    dimensions:     Dimensions;
    weight:         string;
    categories:     Category[];
}

export interface Category {
    id: number;
}

export interface Dimensions {
    length: string;
    width:  string;
    height: string;
}
