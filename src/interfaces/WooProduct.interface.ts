//Objeto con el que responde Woocommerce despues de subir algun producto

export interface WooProductI {
    create: Create[];
    update?: Create[];
    delete?: Create[];
}

export interface Create {
    id:                    number;
    name:                  string;
    slug?:                  string;
    permalink?:             string;
    date_created?:          Date;
    date_created_gmt?:      Date;
    date_modified?:         Date;
    date_modified_gmt?:     Date;
    type?:                  string;
    status?:                string;
    featured?:              boolean;
    catalog_visibility?:    string;
    description?:           string;
    short_description?:     string;
    stock_total:           number;
    sku:                   string;
    price?:                 string;
    regular_price:         string;
    sale_price?:            string;
    date_on_sale_from?:     null;
    date_on_sale_from_gmt?: null;
    date_on_sale_to?:       null;
    date_on_sale_to_gmt?:   null;
    price_html?:            string;
    on_sale?:               boolean;
    purchasable?:           boolean;
    total_sales?:           number;
    virtual?:               boolean;
    downloadable?:          boolean;
    downloads?:             Download[];
    download_limit?:        number;
    download_expiry?:       number;
    external_url?:          string;
    button_text?:           string;
    tax_status?:            string;
    tax_class?:             string;
    manage_stock:          boolean;
    stock_quantity:        null;
    stock_status?:          string;
    backorders?:            string;
    backorders_allowed?:    boolean;
    backordered?:           boolean;
    sold_individually?:     boolean;
    weight?:                string;
    dimensions?:            Dimensions;
    shipping_required?:     boolean;
    shipping_taxable?:      boolean;
    shipping_class?:        string;
    shipping_class_id?:     number;
    reviews_allowed?:       boolean;
    average_rating?:        string;
    rating_count?:          number;
    related_ids?:           number[];
    upsell_ids?:            any[];
    cross_sell_ids?:        any[];
    parent_id?:             number;
    purchase_note?:         string;
    categories?:            Category[];
    tags?:                  any[];
    images?:                Image[];
    attributes?:            Attribute[];
    default_attributes?:    DefaultAttribute[];
    variations?:            any[];
    grouped_products?:      any[];
    menu_order?:            number;
    meta_data?:             any[];
    _links?:                Links;
}

export interface Links {
    self:       Collection[];
    collection: Collection[];
}

export interface Collection {
    href: string;
}

export interface Attribute {
    id:        number;
    name:      string;
    position:  number;
    visible:   boolean;
    variation: boolean;
    options:   string[];
}

export interface Category {
    id:   number;
    name: string;
    slug: string;
}

export interface DefaultAttribute {
    id:     number;
    name:   string;
    option: string;
}

export interface Dimensions {
    length: string;
    width:  string;
    height: string;
}

export interface Download {
    id:   number;
    name: string;
    file: string;
}

export interface Image {
    id:                number;
    date_created:      Date;
    date_created_gmt:  Date;
    date_modified:     Date;
    date_modified_gmt: Date;
    src:               string;
    name:              string;
    alt:               string;
}
